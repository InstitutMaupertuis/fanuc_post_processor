# How to build the API documentation

Install `rosdoc-lite`:
```bash
sudo apt install -y ros-melodic-rosdoc-lite
```

Generate the documentation:
```bash
cd $(catkin_workspace)/build
make fanuc_post_processor_doc
```
