cmake_minimum_required(VERSION 3.2)
project(fanuc_post_processor)
add_compile_options(-Wall -Wextra)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
  eigen_conversions
  industrial_robot_angle_conversions
  roscpp
  tf
  tf_conversions
)

find_package(CURL REQUIRED)

################################################
## Declare ROS messages, services and actions ##
################################################

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

###################################
## catkin specific configuration ##
###################################

catkin_package(
  INCLUDE_DIRS
  include
  LIBRARIES
  ${PROJECT_NAME}
  ${PROJECT_NAME}_curlite
  ${PROJECT_NAME}_pose
  CATKIN_DEPENDS
  eigen_conversions
  industrial_robot_angle_conversions
  roscpp
  tf
)

###########
## Build ##
###########

include_directories(
  include
  ${CURL_INCLUDE_DIRS}
  ${catkin_INCLUDE_DIRS}
)

# Documentation target
add_custom_target(${PROJECT_NAME}_doc
  rosdoc_lite -o ${CATKIN_DEVEL_PREFIX}/documentation/${PROJECT_NAME} ${CATKIN_DEVEL_PREFIX}/../src/${PROJECT_NAME}/${PROJECT_NAME}
)

# Curlite
add_library(
  ${PROJECT_NAME}_curlite
  src/curlite.cpp
)
target_link_libraries(
  ${PROJECT_NAME}_curlite
  ${CURL_LIBRARIES}
)

# Pose library
add_library(
  ${PROJECT_NAME}_pose
  src/pose.cpp
)
target_link_libraries(
  ${PROJECT_NAME}_pose
  ${catkin_LIBRARIES}
)

# Fanuc post processor
add_library(
  ${PROJECT_NAME}
  src/post_processor.cpp
)
target_link_libraries(
  ${PROJECT_NAME}
  ${CURL_LIBRARIES}
  ${PROJECT_NAME}_curlite
  ${PROJECT_NAME}_pose
)
add_dependencies(
  ${PROJECT_NAME}
  ${PROJECT_NAME}_pose
)

#############
## Install ##
#############

## Mark executables and/or libraries for installation
install(TARGETS
  ${PROJECT_NAME}
  ${PROJECT_NAME}_curlite
  ${PROJECT_NAME}_pose
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

## Mark cpp header files for installation
install(
  DIRECTORY
  include/${PROJECT_NAME}/
  DESTINATION
  ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
)

#############
## Testing ##
#############

if(CATKIN_ENABLE_TESTING)
  find_package(rostest REQUIRED)
  add_rostest_gtest(
    ${PROJECT_NAME}_test
    test/program.launch
    test/program.cpp
  )
  target_link_libraries(
    ${PROJECT_NAME}_test
    ${PROJECT_NAME}
    ${catkin_LIBRARIES}
  )
  add_dependencies(
    ${PROJECT_NAME}_test
    ${PROJECT_NAME}
    ${catkin_EXPORTED_TARGETS}
  )
endif()
