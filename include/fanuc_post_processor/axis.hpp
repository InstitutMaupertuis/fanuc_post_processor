#ifndef FANUC_POST_PROCESSOR_AXIS_HPP
#define FANUC_POST_PROCESSOR_AXIS_HPP

#include <string>

namespace fanuc_post_processor
{

class FanucAxis
{
public:
  enum class AxisType
  {
    LINEAR,
    ROTATIVE
  };

  FanucAxis(const AxisType type,
            const std::string name,
            const double value = 0) :
          type_(type),
          name_(name),
          value_(value)
  {
  }

  FanucAxis(const FanucAxis& other) :
          type_(other.type_),
          name_(other.name_),
          value_(other.value_)
  {
  }

  FanucAxis& operator=(const FanucAxis& other)
  {
    value_ = other.value_;
    return *this;
  }

  virtual ~FanucAxis()
  {
  }

  const AxisType type_;
  const std::string name_;
  double value_;
};

}
#endif
