#ifndef FANUC_POST_PROCESSOR_COMMAND_HPP
#define FANUC_POST_PROCESSOR_COMMAND_HPP

#include <fanuc_post_processor/pose.hpp>
#include <string>

namespace fanuc_post_processor
{

class FanucCommand
{
public:
  FanucCommand()
  {
  }

  virtual ~FanucCommand()
  {
  }
};

class FanucEndProgram : public FanucCommand
{
public:
  FanucEndProgram(const bool call_next) :
    call_next_(call_next)
  {
  }

  virtual ~FanucEndProgram()
  {
  }

  bool call_next_;
};

class FanucStringCommand : public FanucCommand
{
public:
  FanucStringCommand(const std::string cmd) :
          cmd_(cmd)
  {
  }

  virtual ~FanucStringCommand()
  {
  }

  std::string cmd_;
};

class FanucPoseCommand : public FanucCommand
{
public:
  FanucPoseCommand(const FanucPose &pose) :
          pose_(pose)
  {
  }

  virtual ~FanucPoseCommand()
  {
  }

  FanucPose pose_;
};

}
#endif
