#ifndef FANUC_POST_PROCESSOR_GROUP_HPP
#define FANUC_POST_PROCESSOR_GROUP_HPP

#include <fanuc_post_processor/axis.hpp>
#include <vector>

namespace fanuc_post_processor
{

class FanucGroup
{
public:
  FanucGroup(const std::vector<FanucAxis> axis,
             const unsigned user_frame = 0,
             const unsigned tool_frame = 1) :
          axis_(axis),
          user_frame_(user_frame),
          tool_frame_(tool_frame)
  {
  }

  virtual ~FanucGroup()
  {
  }

  std::vector<FanucAxis> axis_;
  unsigned user_frame_;
  unsigned tool_frame_;
};

}
#endif
